-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           5.7.24 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour portfolio
CREATE DATABASE IF NOT EXISTS `portfolio` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `portfolio`;

-- Listage de la structure de la table portfolio. experience
CREATE TABLE IF NOT EXISTS `experience` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime DEFAULT NULL,
  `is_current` tinyint(1) DEFAULT NULL,
  `enterprise` varchar(255) NOT NULL,
  `location` varchar(255) DEFAULT NULL,
  `description` text,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Listage des données de la table portfolio.experience : ~2 rows (environ)
DELETE FROM `experience`;
/*!40000 ALTER TABLE `experience` DISABLE KEYS */;
INSERT INTO `experience` (`id`, `name`, `date_start`, `date_end`, `is_current`, `enterprise`, `location`, `description`, `user_id`) VALUES
	(1, 'XP 1', '2020-01-01 16:54:18', '2020-04-30 16:54:18', NULL, 'Chez oim', 'Saint Lô', 'Je faisais le café', 1),
	(2, 'Conseilleur Pole emploi', '2020-06-01 16:54:18', '2020-06-05 16:54:18', NULL, 'Pole Emploi', 'Saint lô', 'Je pointe', 1);
/*!40000 ALTER TABLE `experience` ENABLE KEYS */;

-- Listage de la structure de la table portfolio. formation
CREATE TABLE IF NOT EXISTS `formation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `formation_level` varchar(255) DEFAULT NULL,
  `date_start` datetime NOT NULL,
  `year` varchar(4) DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `school` varchar(255) NOT NULL,
  `is_graduate` tinyint(1) NOT NULL DEFAULT '0',
  `description` text,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Listage des données de la table portfolio.formation : ~0 rows (environ)
DELETE FROM `formation`;
/*!40000 ALTER TABLE `formation` DISABLE KEYS */;
INSERT INTO `formation` (`id`, `name`, `formation_level`, `date_start`, `year`, `date_end`, `school`, `is_graduate`, `description`, `user_id`) VALUES
	(1, 'Certificat de navigation Internet', NULL, '2020-04-01 12:08:23', '2020', '2020-06-01 12:08:23', 'Afpa', 1, 'Certificat de navigation Internet: <br>\r\n<ul>\r\n<li>\r\nJ\'ai appris à envoyer des mails\r\n</li>\r\n<li>\r\nA faire une recherche sur Google\r\n</li>\r\n</ul>\r\n<p> c\'était très intéressant</p>', 1);
/*!40000 ALTER TABLE `formation` ENABLE KEYS */;

-- Listage de la structure de la table portfolio. realisation
CREATE TABLE IF NOT EXISTS `realisation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `year` varchar(4) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Listage des données de la table portfolio.realisation : ~0 rows (environ)
DELETE FROM `realisation`;
/*!40000 ALTER TABLE `realisation` DISABLE KEYS */;
INSERT INTO `realisation` (`id`, `name`, `description`, `year`, `url`, `user_id`) VALUES
	(1, 'Mon cv en ligne', 'c\'est mon cv en HTML', '2018', 'http://benoit-mottin.com', 1),
	(2, 'Mon cv pdf', 'c\'est mon cv en PDF', '2015', 'www.site.com', 1);
/*!40000 ALTER TABLE `realisation` ENABLE KEYS */;

-- Listage de la structure de la table portfolio. realisation_image
CREATE TABLE IF NOT EXISTS `realisation_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `alt` varchar(255) NOT NULL,
  `realisation_id` int(11) NOT NULL,
  `date_upload` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Listage des données de la table portfolio.realisation_image : ~2 rows (environ)
DELETE FROM `realisation_image`;
/*!40000 ALTER TABLE `realisation_image` DISABLE KEYS */;
INSERT INTO `realisation_image` (`id`, `name`, `alt`, `realisation_id`, `date_upload`) VALUES
	(1, 'real1.png', 'Mon cv hTML', 1, '2020-06-15 14:38:21'),
	(2, 'real2.png', 'Ma gestion du projet', 1, '2020-06-15 14:38:21');
/*!40000 ALTER TABLE `realisation_image` ENABLE KEYS */;

-- Listage de la structure de la table portfolio. skill
CREATE TABLE IF NOT EXISTS `skill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `year` varchar(4) NOT NULL,
  `skill_level` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Listage des données de la table portfolio.skill : ~2 rows (environ)
DELETE FROM `skill`;
/*!40000 ALTER TABLE `skill` DISABLE KEYS */;
INSERT INTO `skill` (`id`, `name`, `logo`, `year`, `skill_level`, `user_id`) VALUES
	(1, 'PHP', 'devicon-php-plain', '4', 4, 1),
	(2, 'HTML', 'devicon-html5-plain', '2020', 4, 1);
/*!40000 ALTER TABLE `skill` ENABLE KEYS */;

-- Listage de la structure de la table portfolio. user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `biography` text,
  `email` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `city` varchar(50) NOT NULL,
  `postal_code` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Listage des données de la table portfolio.user : ~0 rows (environ)
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `forname`, `lastname`, `avatar`, `biography`, `email`, `phone`, `password`, `city`, `postal_code`) VALUES
	(1, 'Benoît', 'MOTTIN', './public/image/avatar.png', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet vel accusantium ex nobis commodi, accusamus debitis omnis porro obcaecati voluptatibus. Fugit alias nostrum itaque ut? Atque, accusamus maxime, error deleniti, nihil dicta ullam expedita aliquam distinctio quis temporibus! Placeat iste inventore perferendis consequatur quisquam illo voluptas cupiditate eligendi voluptatem molestias!', 'benoit.mottin@normandie.cci.fr', '07.64.48.78.11', 'admin123', 'Saint Lô', '50000');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
