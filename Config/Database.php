<?php

class Database{

    /* ***********************
     * les accès à la BDD
     * ********************* */
    private $server = "localhost";
    private $user = "root";
    private $password = "";
    private $dbname = "portfolio";
    private $port = "3306";

    // l'objet connextion qui va contenir notre connxtion à la BDD
    public $conn;

    /*
     *  Je creer une nouvelle connection à la BDD à chaque instanciation
     * d'une instance de type Database 
     */
    public function __construct(){
        $this->getConnection();
    }

    /*
     * Au moment de la destuction de mon objet connetion
     * on détruit l'installa en la mettant à null
     */
    public function __destruct() {
        $this->conn = null;
    }

    /**
     * Recuparation de la connextion à notre BDD
     */
    public function getConnection(){

        // reinitialise la connection pour éviter de saturer le serveur de BDD
        $this->conn = null;
        try {
            $this->conn = new PDO(
                "mysql:host=" . $this->server . ";port=" . $this->port . ";dbname=" . $this->dbname, $this->user, $this->password
            );
            $this->conn->exec("set names utf8");
        } catch (Exception $e) {
            echo "ERREUR :" . $e->getMessage();
        } finally {
            // TODO ajouter un fichier de log
            // echo "Je passe tout le temps dans le finally";
        }
        
        
    }
}
