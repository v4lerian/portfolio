<?php

// je vérifie que la valeur existe dans la requête (envoie depuis le formulaire)
// et que ça valeur n'est pas vide
if(isset($_POST['forname']) && $_POST['forname'] != ""){
    $user->setForname($_POST['forname']);
} 
 
if(isset($_POST['lastname'])  && $_POST['lastname'] != ""){
    $user->setLastname($_POST['lastname']);
} 

if(isset($_POST['phone'])  && $_POST['phone'] != ""){
    $user->setPhone($_POST['phone']);
} 

if(isset($_POST['city'])  && $_POST['city'] != ""){
    $user->setCity($_POST['city']);
} 

if(isset($_POST['postal_code'])  && $_POST['postal_code'] != ""){
    $user->setPostalCode($_POST['postal_code']);
} 

if(isset($_POST['biography'])  && $_POST['biography'] != ""){
    $user->setBiography($_POST['biography']);
} 

// var_dump($user);

// persister l'info dans la base
$uc->updateUser($user);


header("Location:?action=home");