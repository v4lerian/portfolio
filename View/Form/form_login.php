<div class="container">

    <form action="?action=proceed_login" method="post">
        <fieldset>
            <legend>Se connecter</legend>
            
            <input type="email" name="login_email" required class="form-control-plaintext bg-primary  mt-2" id="" placeholder="email">
            <input type="password" name="login_pass" required class="form-control-plaintext bg-primary  mt-2" id="" placeholder="Mot de passe">

            <button type="submit" class="btn btn-primary  mt-2">
                Se connecter
            </button>
    </form>

</div>