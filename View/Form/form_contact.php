<div class="container">

    <form action="?action=proceed_contact" method="post">
        <fieldset>
            <legend>Contactez Moi</legend>
            <p>
                Je suis ouvert à toute mission IT, n'hésitez pas à me contacter
            </p>

            <select name="contact_type" class="form-control-plaintext bg-primary  mt-2" id="" >
                <option disabled selected hidden value="">Type de demande</option>
                <option value="contact">Demande généraliste</option>
                <option value="mission">Proposition de mission</option>
            </select>

            <input type="email" name="contact_email" class="form-control-plaintext bg-primary  mt-2" id="" placeholder="email">
            <textarea name="contact_content" class="form-control bg-primary mt-2" id="" cols="30" rows="10"></textarea>

            <button type="submit" class="btn btn-primary mt-2">
                Envoyer
            </button>
    </form>

</div>