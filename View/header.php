<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Utilisation code php -->
    <!-- 
    <title>Mon Portfolio - <?php echo $user->getForname() . " " . $user->getLastname() ?></title>
    -->
    <!-- affiche le contenu de ma variable grâce au shorttag -->
    <title>Mon Portfolio - <?= $user->getForname() ?> <?= $user->getLastname() ?></title>

    <link rel="stylesheet" href="./public/style/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.rawgit.com/konpa/devicon/df6431e323547add1b4cf45992913f15286456d3/devicon.min.css">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="?action=home">Mon Portfolio</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="?action=home">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?action=home#xp">Expériences</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?action=home#form">Formations</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?action=home#skill">Compétences</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?action=realisations">Réalisations</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?action=contact">Contact</a>
                </li>

                <?php if (isset($_SESSION['user_lastname'])) { ?>
                    <li class="nav-item">
                        <a class="nav-link" href="?action=proceed_logout">Se déconnecter</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?action=admin">Administrer</a>
                    </li>
                <?php } else { ?>
                    <li class="nav-item">
                        <a class="nav-link" href="?action=login">Se connecter</a>
                    </li>
                <?php }  ?>

            </ul>
            <!-- TODO acces backoffice -->
        </div>
    </nav>