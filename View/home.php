<div class="container">
    <div class="jumbotron">
        <div class="d-flex justify-content-center">
            <?php if ($user->getAvatar()) { ?>
                <img src="<?= $user->getAvatar() ?>" alt="Avatar <?= $user->getForname() ?> <?= $user->getLastname() ?>">
            <?php } ?>
        </div>
        <div class="d-flex justify-content-center">
            <h1 class="display-3"><?= $user->getForname() ?> <?= $user->getLastname() ?></h1>
        </div>
        <hr class="my-4">
        <p><?= $user->getBiography() ?></p>
        <p class="lead">
            <!-- TODO add link to contact form -->
            <a class="btn btn-primary btn-lg" href="#" role="button">ContacteZ Moi</a>
        </p>
    </div>
    <section id="xp">
        <h2>Expériences</h2>
        <!-- TODO afficher toutes les expériences -->
        <ul class="list-group">
            <?php
            
            $exp_str = "";
            foreach ($experiences as $key => $xp) {
                $exp_str .= '
                    <li class="list-group-item">
                        <h3>' . $xp->getName() . '</h3>
                        <span class="badge badge-primary badge-pill">
                            ' . $xp->getEnterprise() .' - '. $xp->getLocation() .' </span>
                            <!-- FIXME mauvaise date si pas de date_end -->';
                // si c'est notre poste actuel
                if($xp->getIsCurrent()){
                    $exp_str .= '               
                    <span> depuis '. $xp->getDateStart() .' à Aujourd\'hui</span>
                    ';
                } else {                    
                $exp_str .= '               
                        <span>Du '. $xp->getDateStart() .' au '. $xp->getDateEnd() .'</span>
                        ';
                }
                $exp_str .= '               
                        <div>
                            <p>'. $xp->getDescription() .'</p>
                        </div>
                    </li>';
            }
            
            echo $exp_str;
            ?>
        </ul>
    </section>
    <section id="form">
        <h2>Formations</h2>
        <div class="card-deck">
        <?php
        $form_str = "";

        foreach ($formations as $key => $form) {
            $form_str .= '
                <div class="card text-white bg-primary mb-3" style="max-width: 20rem;">
                    <div class="card-header">'. $form->getName() .'</div>
                    <div class="card-body">
                        <h4 class="card-title">' . $form->getSchool() . '</h4>
                        <p class="card-text">'. $form->getDescription() .'</p>
                    </div>
                </div>
            ';
        }
        echo $form_str;
        ?>
        </div>
        <!-- TODO afficher toutes les formations -->
    </section>
    <section id="skill">
        <h2>Compétences</h2>
        <!-- TODO afficher toutes les formations -->

        <?php

        $skill_str = "";

        foreach ($skills as $key => $skill) {
            $skill_str .= '
                <button 
                    type="button" 
                    class="btn btn-secondary" 
                    title="' . $skill->getName() . '" 
                    data-container="body" 
                    data-toggle="popover" 
                    data-placement="left" 
                    data-content="' . $skill->getName() . '" data-original-title="' . $skill->getName() . '">
                    <i class="display-4 '. $skill->getLogo() .'"></i>
                </button>

            ';
        }
        echo $skill_str;
        ?>
        
    </section>

</div>