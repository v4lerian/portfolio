<div class="container-fluid mt-4">
    <div class="row">
        <div class="col-2 bg-secondary">
            <ul>
                <li>
                    <a href="?action=admin&sub=profil">Profil</a>
                </li>
                <li>
                    <a href="?action=admin&sub=xp">Expériences</a>
                </li>
                <li>
                    <a href="?action=admin&sub=formation">Formations</a>
                </li>
                <li>
                    <a href="?action=admin&sub=skill">Compétences</a>
                </li>
                <li>
                    <a href="?action=admin&sub=realisation">Réalisations</a>
                </li>
            </ul>
        </div>
        <div class="col-9 offset-1 bg-secondary">
            <?= $page ?> 
        </div>
    </div>
</div>