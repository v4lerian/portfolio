<h2>
    Gestion du profil
</h2>

<form action="?action=update_profil" method="POST">

    <input type="text" name="forname" placeholder="Prénom" id="">
    <input type="text" name="lastname" placeholder="Nom" class="mb-2" id="">
    <input type="tel" name="phone" placeholder="Téléphone" class="form-control mb-2" id="">
    <input type="text" name="city" placeholder="Ville" class="form-control mb-2" id="">
    <input type="text" name="postal_code" placeholder="Code Postal" class="form-control mb-2" id="">
    <textarea name="biography" id="editor1" class="form-control" cols="30" rows="10">
    </textarea>
    <input type="submit" class="btn btn-primary mt-2 mb-2" value="Mettre à jour">
</form>

<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<script>
    // Replace the <textarea id="editor1"> with a CKEditor 4
    // instance, using default configuration.
    CKEDITOR.replace('editor1');
</script>