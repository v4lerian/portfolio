<div class="container">
   <h2>Réalisations</h2>
<!-- 
- creer les routes (tous et produit)

- On va recupérer la liste des real

-/ on va boucler dessus en affichant 
    le titre
    un résumé (50 caracteres)
    un CTA => page produit (getRealisationById)

-  le update dans la BDD
-->

<ul class="list-group">
            <?php
                $realisationController = new RealisationController();
                $realisations = $realisationController->getRealisations();        
            
            $real_str = "";
            foreach ($realisations as $key => $real) {
                $real_str .= '
                <div class="alert alert-primary">
                <h4 class="alert-heading">' . $real->getName() .'</h4>
                <p>' . $real->getDescription() .'</p>
                <a class="btn btn-secondary" href="?action=realisation">Voir ça</a>
                <hr>
                <p class="mb-0">'. $real->getYear() . '</p>
              </div>';
                }
            
            echo $real_str;
            ?>
        </ul>

</div>