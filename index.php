<?php
// toujours en premier
session_start();

// TODO DEBUG 
//var_dump($_SESSION);

/* *****************
 * Require
 */
// require importe le fichier indiqué, mais stop le programme s'il ne le trouve pas
// require "./Model/User.php";
// inclus le fichier, mais continu le script s'il ne le trouve
// include "./Model/User.php";
// inclus le fichier, mais continu le script s'il ne le trouve et ne l'importe qu'une fois en cas d'import multiple
// include_once "./Model/User.php";

// CONFIG
require_once './Config/Database.php';

// comme require mais l'mporte qu'une fois

// MODEL
require_once "./Model/User.php";
require_once "./Model/Experience.php";
require_once "./Model/Formation.php";
require_once "./Model/Realisation.php";
require_once "./Model/RealisationImage.php";
require_once "./Model/Skill.php";
// CONTROLLER
require_once "./Controller/UserController.php";
require_once "./Controller/ExperienceController.php";
require_once "./Controller/FormationController.php";
require_once "./Controller/RealisationController.php";
require_once "./Controller/RealisationImageController.php";
require_once "./Controller/SkillController.php";


/* *******************************
 * Route
 *  */
if (isset($_GET['action'])) {
    // si oui je rentre dans le if
    // et je remplis la valeur
    $action = $_GET['action'];
} else {
    $action = 'home';
}

// TODO ajouter une combobox pour changer d'utilisateur et charger un autre CV
// instancie un nouveau controller
$uc = new UserController();
// et je recuperer l'utilisateur qui as l'id 
if (!isset($user)) {
    $user = $uc->getUserById(1);
}

// var_dump($user);
/* ********************
 * ROUTE + VIEW
 * *******************/
// On affiche toujours le menu
require_once './View/header.php';

switch ($action) {

    case 'home':
        // recuperation des experiences depuis la BDD
        $experienceController = new ExperienceController();
        $experiences = $experienceController->getExperiences();

        // les formations
        $formationController = new FormationController();
        $formations = $formationController->getFormations();
        // les compétences
        $skillController = new SkillController();
        $skills = $skillController->getSkills($user->getId());
        // affiche la page d'accueil
        require_once './View/home.php';
        break;
    case 'realisations':
        require_once "./View/realisations.php";
        break;
    case 'realisation':
        require_once "./View/realisation.php";
        break;
    case 'contact':
        // affiche le formulaire de contact
        require_once "./View/Form/form_contact.php";
        break;
    case 'login':
        // afficher login form
        require_once './View/Form/form_login.php';
        break;
    case 'admin':

        if (isset($_GET['sub'])) {
            $sub = $_GET['sub'];
        } else {
            $sub = 'profil';
        }

        switch ($sub) {
            case 'profil':
                // TODO get the php evaluated from this file
                $page = file_get_contents('./View/Admin/Form/form_profil.php');
                break;

            default:
                # code...
                break;
        }

        require_once './View/Admin/admin.php';
        break;
        // ACTION
    case 'proceed_contact':
        require_once './Action/proceed_contact.php';
        break;
    case 'proceed_login':
        require_once './Action/proceed_login.php';
        break;
    case 'proceed_logout':
        // tuer la session
        require_once './Action/proceed_logout.php';
        break;
    case 'update_profil':
        require_once './Action/proceed_profil_update.php';
        break;

    default:
        // TODO afficher 404
        require_once './View/404.php';

        break;
}

// On affiche toujours le footer et la fin de page
require_once './View/footer.php';
