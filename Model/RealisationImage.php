<?php

class RealisationImage
{

    private $id;
    private $name;
    private $alt;
    private $realisation;
    private $date_upload;

    /* ************
     * GETTER/SETTER
     * *************/

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of alt
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * Set the value of alt
     *
     * @return  self
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;

        return $this;
    }

    /**
     * Get the value of realisation
     */
    public function getRealisation()
    {
        return $this->realisation;
    }

    /**
     * Set the value of realisation
     *
     * @return  self
     */
    public function setRealisation($realisation)
    {
        $this->realisation = $realisation;

        return $this;
    }

    /**
     * Get the value of date_upload
     */
    public function getDateUpload()
    {
        return $this->date_upload;
    }

    /**
     * Set the value of date_upload
     *
     * @return  self
     */
    public function setDateUpload($date_upload)
    {
        $this->date_upload = $date_upload;

        return $this;
    }
}
