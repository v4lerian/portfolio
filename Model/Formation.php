<?php

class Formation{

    // attributes
    private $id;
    private $name;
    private $formation_level;
    private $date_start;
    private $year;
    private $date_end;
    private $school;
    private $is_graduate;
    private $description;   
    private $user;
 
    /* **************
     *  GETTER/SETTERZ
     * */

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of date_start
     */ 
    public function getDateStart()
    {
        return $this->date_start;
    }

    /**
     * Set the value of date_start
     *
     * @return  self
     */ 
    public function setDateStart($date_start)
    {
        $this->date_start = $date_start;

        return $this;
    }

    /**
     * Get the value of year
     */ 
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set the value of year
     *
     * @return  self
     */ 
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get the value of date_end
     */ 
    public function getDateEnd()
    {
        return $this->date_end;
    }

    /**
     * Set the value of date_end
     *
     * @return  self
     */ 
    public function setDateEnd($date_end)
    {
        $this->date_end = $date_end;

        return $this;
    }

    /**
     * Get the value of school
     */ 
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * Set the value of school
     *
     * @return  self
     */ 
    public function setSchool($school)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get the value of is_graduate
     */ 
    public function getIsGraduate()
    {
        return $this->is_graduate;
    }

    /**
     * Set the value of is_graduate
     *
     * @return  self
     */ 
    public function setIsGraduate($is_graduate)
    {
        $this->is_graduate = $is_graduate;

        return $this;
    }

    /**
     * Get the value of description
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */ 
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of user
     */ 
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set the value of user
     *
     * @return  self
     */ 
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get the value of formation_level
     */ 
    public function getFormationLevel()
    {
        return $this->formation_level;
    }

    /**
     * Set the value of formation_level
     *
     * @return  self
     */ 
    public function setFormationLevel($formation_level)
    {
        $this->formation_level = $formation_level;

        return $this;
    }
}