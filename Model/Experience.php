<?php

class Experience {

    private $id;
    private $name;
    private $date_start;
    private $is_current;
    private $date_end;
    private $enterprise;
    private $location;
    private $description;
    private $user;

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of date_start
     */ 
    public function getDateStart($raw=false)
    {
        if($raw){
            return $this->date_start;
        } else {
            $d = new DateTime($this->date_start);
            return $d->format('m-Y');
        }

        
    }

    /**
     * Set the value of date_start
     *
     * @return  self
     */ 
    public function setDateStart($date_start)
    {
        $this->date_start = $date_start;

        return $this;
    }

    /**
     * Get the value of is_current
     */ 
    public function getIsCurrent()
    {
        return $this->is_current;
    }

    /**
     * Set the value of is_current
     *
     * @return  self
     */ 
    public function setIsCurrent($is_current)
    {
        $this->is_current = $is_current;

        return $this;
    }

    /**
     * Get the value of date_end
     */ 
    public function getDateEnd($raw=false)
    {
        if($raw){
            return $this->date_end;
        } else {
            $d = new DateTime($this->date_end);
            return $d->format('m-Y');
        }
    }

    /**
     * Set the value of date_end
     *
     * @return  self
     */ 
    public function setDateEnd($date_end)
    {
        $this->date_end = $date_end;

        return $this;
    }

    /**
     * Get the value of enterprise
     */ 
    public function getEnterprise()
    {
        return $this->enterprise;
    }

    /**
     * Set the value of enterprise
     *
     * @return  self
     */ 
    public function setEnterprise($enterprise)
    {
        $this->enterprise = $enterprise;

        return $this;
    }

    /**
     * Get the value of location
     */ 
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set the value of location
     *
     * @return  self
     */ 
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get the value of description
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */ 
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of user
     */ 
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set the value of user
     *
     * @return  self
     */ 
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }
}