<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.rawgit.com/konpa/devicon/df6431e323547add1b4cf45992913f15286456d3/devicon.min.css">

</head>
<body>
<?php

// CONFIG
require_once './Config/Database.php';

// comme require mais l'mporte qu'une fois

// MODEL
require_once "./Model/User.php";
require_once "./Model/Experience.php";
require_once "./Model/Formation.php";
require_once "./Model/Realisation.php";
require_once "./Model/RealisationImage.php";
require_once "./Model/Skill.php";
// CONTROLLER
require_once "./Controller/UserController.php";
require_once "./Controller/ExperienceController.php";
require_once "./Controller/FormationController.php";
require_once "./Controller/RealisationController.php";
require_once "./Controller/RealisationImageController.php";
require_once "./Controller/SkillController.php";

$ec = new ExperienceController();

echo "resultat get XP<br><pre>";
$xps = $ec->getExperiences();
// var_dump($xps);
echo "</pre>";


echo "--------------------------- <br> User";

echo $xps[0]->getUser()->getEmail();

$uc = new UserController();
$user = $uc->getUserById(1);
echo "<br><pre>";
var_dump($user);
echo "</pre>";

echo "--------------------------- <br> Formation";

$uf = new FormationController();
$form = $uf->getFormationById(1);
echo $form->getName();
echo $form->getUser()->getForname();

echo "<pre>";

var_dump($uf->getFormations());
echo "</pre>";


echo "--------------------------- <br> Realisation ";
$ur = new RealisationController();
$real = $ur->getRealisationById(1);

echo "<pre>";

var_dump($real);
echo "</pre>";


echo "<pre>";

var_dump($ur->getRealisations());
echo "</pre>";

echo "--------------------------- <br> RealisationImage ";

$ric = new RealisationImageController();
$images = $ric->getImageByRealisationId($real->getId());
echo "<pre>";
var_dump($images);
echo "</pre>";
foreach ($images as $key => $image) {   
    echo "<img src='./public/image/" . $image->getName()."' alt='".$image->getAlt()."'>";
} //*/


echo "--------------------------- <br> Skills ";
$sc = new SkillController();
$skills = $sc->getSkills($user->getId());

foreach ($skills as $key => $skill) {
 echo '<i class="'. $skill->getLogo() .'" title="' . $skill->getName() . '"></i>';
}

/*
echo '<li class="list-group-item d-flex justify-content-between align-items-center">';
                echo $xp->getName();
                echo '<span class="badge badge-primary badge-pill">14</span>
        </li>';
//*/
?>

<!-- in your body -->
<i class="devicon-amazonwebservices-original"></i>

