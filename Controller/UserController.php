<?php

class UserController
{

    public $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function getUserById($id)
    {
        // définition de la requête
        $query = "
            SELECT 
                *
            FROM
                user
            WHERE 
               id = :id
        ";
        // je prepare ma requete; le moteur de PHP prepare une requete à trou
        $stmt = $this->db->conn->prepare($query);

        // PHP va mettre les valeurs fourni dans les trous
        $stmt->execute([':id' => $id]);

        // je recupere les info depuis la BDD, fetch(PDO::FETCH_ASSOC) me retourne un tableau ASSOCIATIF
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        // les info recuperer vont ressembler à ça

        // $row = ['id'=> 1, 'forname'=> 'Benoit', 'lastname'=> 'MOTTIN']

        // j'instancie un nouvel objet User
        $user = new User();
        // et je le peuple
        $user->setId($row['id']);
        $user->setForname($row['forname']);
        $user->setLastname($row['lastname']);
        $user->setEmail($row['email']);
        $user->setPhone($row['phone']);
        $user->setCity($row['city']);
        $user->setPostalCode($row['postal_code']);
        $user->setAvatar($row['avatar']);
        $user->setBiography($row['biography']);
        // FIXME encrypt password
        $user->setPassword($row['password']);

        // je le retourne 
        return $user;
    }

    public function getUserForLogin($login, $pass)
    {
        $query = "
            SELECT 
                *
            FROM
                user
            WHERE 
               email = :login
            AND
                password = :pass
        ";
        // je prepare ma requete; le moteur de PHP prepare une requete à trou
        $stmt = $this->db->conn->prepare($query);

        // PHP va mettre les valeurs fourni dans les trous
        $stmt->execute([':login' => $login, 'pass' => $pass]);

        // je recupere les info depuis la BDD, fetch(PDO::FETCH_ASSOC) me retourne un tableau ASSOCIATIF
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        // Si les info corresponde à un utilsateur existant
        if ($stmt->rowCount() > 0) {
            
            // j'instancie un nouvel objet User
            $user = new User();
            // et je le peuple
            $user->setId($row['id']);
            $user->setForname($row['forname']);
            $user->setLastname($row['lastname']);
            $user->setEmail($row['email']);
            $user->setPhone($row['phone']);
            $user->setCity($row['city']);
            $user->setPostalCode($row['postal_code']);
            $user->setAvatar($row['avatar']);
            $user->setBiography($row['biography']);
            // FIXME encrypt password
            $user->setPassword($row['password']);

            // je le retourne 
            return $user;
        }
    }

    public function updateUser($user) {
        $query = "
            UPDATE 
                user
            SET
                forname = :forname,
                lastname = :lastname,
                phone = :phone, 
                city = :city,
                postal_code = :post,
                biography = :bio
            WHERE
                id = :id
        ";

        // je prepare ma requete; le moteur de PHP prepare une requete à trou
        $stmt = $this->db->conn->prepare($query);

        // PHP va mettre les valeurs fourni dans les trous
        // des requetes préparées
        return $stmt->execute([
            ':lastname' => $user->getLastname(),
            ':forname' => $user->getForname(), 
            ':phone' => $user->getPhone(),
            ':city' => $user->getCity(),
            ':post' => $user->getPostalCode(),
            ':bio' => $user->getBiography(),
            ':id' => $user->getId()
            ]);
        
    }

    // TODO ajouter le get All pour choisir le profile à afficher
}
