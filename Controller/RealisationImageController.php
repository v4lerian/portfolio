<?php

class RealisationImageController
{

    public $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function getImageByRealisationId($id)
    {
        // définition de la requête
        $query = "
            SELECT 
                *
            FROM
                realisation_image
            WHERE 
               realisation_id = :id
        ";
        // je prepare ma requete; le moteur de PHP prepare une requete à trou
        $stmt = $this->db->conn->prepare($query);

        // PHP va mettre les valeurs fourni dans les trous
        $stmt->execute([':id' => $id]);

        $images = [];
        // compter le nombre de ligne
        if ($stmt->rowCount() > 0) {
            // je recupere ;une collection d'object
            $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
            // je parcours la collection (boucle)
            foreach ($row as $key => $i) {
                echo "----deb--------";
                var_dump($row);
                echo "------fin------";
                $img = new RealisationImage();
                // et je le peuple
                $img->setId($i['id']);
                $img->setName($i['name']);
                $img->setAlt($i['alt']);
                $img->setDateUpload($i['date_upload']);

                $rc = new RealisationController();

                $img->setRealisation($rc->getRealisationById($i['realisation_id']));
                
                $images[] = $img;
            }
        }

        var_dump($images);
       return $images;
     
    }

}
