<?php

class SkillController
{

    public $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function getSkills($id)
    {
        // preparer la requete
        $query = "
       SELECT 
           *
       FROM
           skill     
   ";

        // executer la requete
        $stmt = $this->db->conn->prepare($query);
        $stmt->execute();

        $skills = [];
        // compter le nombre de ligne
        if ($stmt->rowCount() > 0) {
            // je recupere ;une collection d'object
            $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
            // je parcours la collection (boucle)
            foreach ($row as $key => $s) {

                $skill = new Skill();
                // et je le peuple
                $skill->setId($s['id']);
                $skill->setName($s['name']);
                $skill->setLogo($s['logo']);
                $skill->setYear($s['year']);
                $skill->setSkillLevel($s['skill_level']);
              
                $uc = new UserController();
                $skill->setUser($uc->getUserById($s['user_id']));
                // je la stocke dans un tableau
                $skills[] = $skill;
            }
        }

        // je retourne mon tableau
        return $skills;
    }
}
