<?php

class ExperienceController
{

    public $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function getExperienceById($id)
    {
        // définition de la requête
        $query = "
            SELECT 
                *
            FROM
                experience
            WHERE 
               id = :id
        ";
        // je prepare ma requete; le moteur de PHP prepare une requete à trou
        $stmt = $this->db->conn->prepare($query);

        // PHP va mettre les valeurs fourni dans les trous
        $stmt->execute([':id' => $id]);

        // je recupere les info depuis la BDD, fetch(PDO::FETCH_ASSOC) me retourne un tableau ASSOCIATIF
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        // les info recuperer vont ressembler à ça

        // $row = ['id'=> 1, 'forname'=> 'Benoit', 'lastname'=> 'MOTTIN']

        $xp = new Experience();
        $xp->setId($row['id']);
        $xp->setName($row['name']);
        $xp->setDescription($row['description']);
        $xp->setDateStart($row['date_start']);
        $xp->setDateEnd($row['date_end']);
        $xp->setIsCurrent($row['is_current']);
        $xp->setEnterprise($row['enterprise']);
        $xp->setLocation($row['location']);
        // je creer une nouvelle instance d'utilisateur
        $uc = new UserController();
        $xp->setUser($uc->getUserById($row['user_id']));      

        // je le retourne 
        return $xp;//*/
    }

    public function getExperiences()
    {
        // preparer la requete
        $query = "
            SELECT 
                *
            FROM
                experience     
        ";

        // executer la requete
        $stmt = $this->db->conn->prepare($query);
        $stmt->execute();

        $xps = [];
        // compter le nombre de ligne
        if ($stmt->rowCount() > 0) {
            // je recupere ;une collection d'XP
            $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
            // je parcours la collection (boucle)
            foreach ($row as $key => $xpc) {
                //  je creer une nouvelle instance XP à chaque tour
                $xp = new Experience();
                $xp->setId($xpc['id']);
                $xp->setName($xpc['name']);
                $xp->setDescription($xpc['description']);
                $xp->setDateStart($xpc['date_start']);
                $xp->setDateEnd($xpc['date_end']);
                $xp->setIsCurrent($xpc['is_current']);
                $xp->setEnterprise($xpc['enterprise']);
                $xp->setLocation($xpc['location']);
                // je creer une nouvelle instance d'utilisateur
                $uc = new UserController();
                $xp->setUser($uc->getUserById($xpc['user_id']));

                // je la stocke dans un tableau
                $xps[] = $xp;
            }
        }

        // je retourne mon tableau
        return $xps;
    }
}
