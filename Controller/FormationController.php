<?php

class FormationController
{

    public $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function getFormationById($id)
    {
        // définition de la requête
        $query = "
            SELECT 
                *
            FROM
                formation
            WHERE 
               id = :id
        ";
        // je prepare ma requete; le moteur de PHP prepare une requete à trou
        $stmt = $this->db->conn->prepare($query);

        // PHP va mettre les valeurs fourni dans les trous
        $stmt->execute([':id' => $id]);

        // je recupere les info depuis la BDD, fetch(PDO::FETCH_ASSOC) me retourne un tableau ASSOCIATIF
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        // les info recuperer vont ressembler à ça

        // $row = ['id'=> 1, 'forname'=> 'Benoit', 'lastname'=> 'MOTTIN']

        // j'instancie un nouvel objet User
        $form = new Formation();
        // et je le peuple
        $form->setId($row['id']);
        $form->setName($row['name']);
        $form->setFormationLevel($row['formation_level']);
        $form->setDateStart($row['date_start']);
        $form->setYear($row['year']);
        $form->setDateEnd($row['date_end']);
        $form->setSchool($row['school']);
        $form->setIsGraduate($row['is_graduate']);
        $form->setDescription($row['description']);

        $uc = new UserController();
        $form->setUser($uc->getUserById($row['user_id']));

        // je le retourne 
        return $form; //*/
    }

    public function getFormations()
    {
        // preparer la requete
        $query = "
            SELECT 
                *
            FROM
                formation     
        ";

        // executer la requete
        $stmt = $this->db->conn->prepare($query);
        $stmt->execute();

        $formations = [];
        // compter le nombre de ligne
        if ($stmt->rowCount() > 0) {
            // je recupere ;une collection d'object
            $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
            // je parcours la collection (boucle)
            foreach ($row as $key => $f) {

                $form = new Formation();
                // et je le peuple
                $form->setId($f['id']);
                $form->setName($f['name']);
                $form->setFormationLevel($f['formation_level']);
                $form->setDateStart($f['date_start']);
                $form->setYear($f['year']);
                $form->setDateEnd($f['date_end']);
                $form->setSchool($f['school']);
                $form->setIsGraduate($f['is_graduate']);
                $form->setDescription($f['description']);

                $uc = new UserController();
                $form->setUser($uc->getUserById($f['user_id']));
                // je la stocke dans un tableau
                $formations[] = $form;
            }
        }

        // je retourne mon tableau
        return $formations;
    }
}
