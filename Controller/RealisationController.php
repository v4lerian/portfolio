<?php

class RealisationController
{

    public $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function getRealisationById($id)
    {
        // définition de la requête
        $query = "
            SELECT 
                *
            FROM
                realisation
            WHERE 
               id = :id
        ";
        // je prepare ma requete; le moteur de PHP prepare une requete à trou
        $stmt = $this->db->conn->prepare($query);

        // PHP va mettre les valeurs fourni dans les trous
        $stmt->execute([':id' => $id]);

        // je recupere les info depuis la BDD, fetch(PDO::FETCH_ASSOC) me retourne un tableau ASSOCIATIF
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        // les info recuperer vont ressembler à ça

        // $row = ['id'=> 1, 'forname'=> 'Benoit', 'lastname'=> 'MOTTIN']

        // j'instancie un nouvel objet User
        $real = new Realisation();
        // et je le peuple
        $real->setId($row['id']);
        $real->setName($row['name']);
        $real->setYear($row['year']);
        $real->setDescription($row['description']);
        $real->setUrl($row['description']);

        $uc = new UserController();
        $user = $uc->getUserById($row['user_id']);
        $real->setUser($user);

        // je le retourne 
        return $real; //*/
    }

    public function getRealisations()
    {
        // preparer la requete
        $query = "
          SELECT 
              *
          FROM
              realisation     
        ";

        // executer la requete
        $stmt = $this->db->conn->prepare($query);
        $stmt->execute();

        $realisations = [];
        // compter le nombre de ligne
        if ($stmt->rowCount() > 0) {
            // je recupere ;une collection d'XP
            $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
            // je parcours la collection (boucle)
            foreach ($row as $key => $realisation) {
                //  je creer une nouvelle instance XP à chaque tour
                $real = new Realisation();
                $real->setId($realisation['id']);
                $real->setName($realisation['name']);
                $real->setDescription($realisation['description']);
                $real->setYear($realisation['year']);
                $real->setUrl($realisation['url']);

                // je creer une nouvelle instance d'utilisateur
                $uc = new UserController();
                $real->setUser($uc->getUserById($realisation['user_id']));

                // je la stocke dans un tableau
                $realisations[] = $real;
            }
        }

        // je retourne mon tableau
        return $realisations;
    }
}
